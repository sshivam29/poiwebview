package com.example.poiwebview;



import java.util.List;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MapsWebView extends Activity {
	WebView webView;
	LocationManager locationManager = null;
	LocationListener loc_listener = null;
	Handler jsHandler = new Handler();
	double lat = -1.0,lon = -1.0;
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		
		setContentView(R.layout.activity_maps_web_view);
		
		webView = (WebView) findViewById(R.id.webview);
		getLocationUpdate();
		webView.getSettings().setJavaScriptEnabled(true);
		
		webView.addJavascriptInterface(new MyJavaScriptMapsInterface(), "Android");
		
		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl("file:///android_asset/JioMaps.html");
		
	}
	
	
	private Location getLastKnownLocation() {
	    List<String> providers = locationManager.getProviders(true);
	    Location bestLocation = null;
	    for (String provider : providers) {
	        Location l = locationManager.getLastKnownLocation(provider);
	        Log.d("last known location"," provider:"+provider+", location: "+l);

	        if (l == null) {
	            continue;
	        }
	        if (bestLocation == null
	                || l.getAccuracy() < bestLocation.getAccuracy()) {
	            Log.d("getLastKnownLocation","found best last known location:"+l);
	            bestLocation = l;
	        }
	    }
	    if (bestLocation == null) {
	        return null;
	    }
	    return bestLocation;
	}

	
	boolean isCaptured = false;
	
	public int getLocationUpdate() {
		 	locationManager = (LocationManager)getSystemService (LOCATION_SERVICE);
		 	
		    Criteria criteria = new Criteria ();
		    String bestProvider = locationManager.getBestProvider (criteria, false);
		    Location location = locationManager.getLastKnownLocation (bestProvider);
		    loc_listener = new LocationListener() {

		        public void onProviderEnabled(String p) {
		        	Log.e("getLocationUpdate","Err------------onProviderEnabled:"+p);
		        }

		        public void onProviderDisabled(String p) {
		        	Log.e("getLocationUpdate","Err------------onProviderDisabled:"+p);
		        }

		        public void onStatusChanged(String p, int status, Bundle extras) {
		        	Log.e("getLocationUpdate","Err------------onStatusChanged:"+p);
		        }

				@Override
				public void onLocationChanged(Location l) {
					// TODO Auto-generated method stub
					if(isCaptured == false)
					{
						lat = l.getLatitude();
						lon = l.getLongitude();
						isCaptured = true;
					}
					Log.e("onLocationChanged","Err------------onLocationChanged lat:"+lat);
					//Toast.makeText(getApplicationContext(), "onLocationChanged Lat:"+lat+"Lon:"+lon, Toast.LENGTH_SHORT).show();
				}      
		    };
		    locationManager.requestLocationUpdates(bestProvider,0 ,0, loc_listener);
		    location = locationManager.getLastKnownLocation (bestProvider); 
		    if(location == null)
		    {
		    	location = getLastKnownLocation();
		    }
		    try
		    {
		        lat = location.getLatitude ();
		        lon = location.getLongitude ();	     
		    }
		    catch (NullPointerException e)
		    {
		    	Log.e("getLocationUpdate","Err------------NullPointerException:"+e.getMessage());
		        lat = -1.0;
		        lon = -1.0;
		    }
		    
		    return 0;
	}

	
	class MyWebViewClient extends WebViewClient{

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			
			}
		}
	final class MyJavaScriptMapsInterface{
		public MyJavaScriptMapsInterface() {
			
		}
		public void runJSLoader()
		{
			jsHandler.post(new Runnable() {
				
				@Override
				public void run() {
					webView.loadUrl("javascript: storePosition();");
					
				}
			});
		}
		@JavascriptInterface
		public double getLongitude()
		{
			 Log.d("sendToAndroid","Lon:"+lon);
			return lon;
		}
		@JavascriptInterface
		public double getLatitude()
		{
			 Log.d("sendToAndroid","Lat:"+lat);
			return lat;
		}
		 @JavascriptInterface
		   public void sendToAndroid(String text) {		     
			 Log.d("sendToAndroid",text);
		      Toast t = Toast.makeText(getApplicationContext(), text, 2000);
		      t.show();
		   }

		@JavascriptInterface
		public void Toast(final String s)
		{
			jsHandler.post(new Runnable() {
							
				@Override
				public void run() {
					Log.d("Toast","text:"+s);
					Toast.makeText(MapsWebView.this,s , Toast.LENGTH_SHORT).show();
					
				}
			});
		}
	}
}
